use crate::error::Error;
use crate::function::Callable;
use crate::value::Value;
use crate::interpreter::Interpreter;

pub struct Print;

impl Print {
    pub fn new() -> Self {
        Self {}
    }
}

impl Callable for Print {
    fn arity(&self) -> usize {
        1
    }

    fn call(&self, args: Vec<Value>, _: &mut Interpreter) -> Result<Value, Error> {
        println!("{}", args.get(0).unwrap());
        Ok(Value::Null)
    }
}
