use crate::error::Error;
use crate::function::Callable;
use crate::interpreter::Interpreter;
use crate::value::Value;

pub struct Quit;

impl Quit {
    pub fn new() -> Self {
        Self {}
    }
}

impl Callable for Quit {
    fn arity(&self) -> usize {
        1
    }

    fn call(&self, args: Vec<Value>, _: &mut Interpreter) -> Result<Value, Error> {
        let ret = match args.get(0).unwrap() {
            Value::Integer(value) => value,
            _ => {
                return Err(Error::RuntimeError(format!(
                    "Exit code must be a positive integer"
                )))
            }
        };
        Err(Error::Exit(*ret))
    }
}
