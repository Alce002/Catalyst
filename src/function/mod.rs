pub mod natives;

use std::cell::RefCell;
use std::rc::Rc;

use crate::ast::*;
use crate::environment::Environment;
use crate::error::Error;
use crate::interpreter::Interpreter;
use crate::value::Value;

pub trait Callable {
    fn arity(&self) -> usize;

    fn call(&self, args: Vec<Value>, interpreter: &mut Interpreter) -> Result<Value, Error>;
}

pub struct Function {
    parent: Rc<RefCell<Environment>>,
    parameters: Vec<String>,
    body: AST,
}

impl Function {
    pub fn new(body: AST, parameters: Vec<String>, parent: Rc<RefCell<Environment>>) -> Self {
        Self {
            parent,
            parameters,
            body,
        }
    }
}

impl Callable for Function {
    fn arity(&self) -> usize {
        self.parameters.len()
    }

    fn call(&self, args: Vec<Value>, interpreter: &mut Interpreter) -> Result<Value, Error> {
        let mut env = Environment::new_scope(self.parent.clone());
        for (parameter, value) in self.parameters.iter().zip(args.iter()) {
            env.define(&parameter, Some(value.clone()));
        }

        let current = interpreter.swap_env(Rc::new(RefCell::new(env)));
        match interpreter.visit(&self.body) {
            Ok(_) => (),
            Err(error) => {
                interpreter.swap_env(current);
                match error {
                    Error::Return(value) => return Ok(value),
                    _ => return Err(error),
                }
            }
        };

        interpreter.swap_env(current);

        Ok(Value::Null)
    }
}
