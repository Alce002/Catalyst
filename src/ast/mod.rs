use crate::token::Token;
use crate::value::Value;

#[derive(Clone)]
pub struct AST {
    token: Token,
    node: Node,
}

impl AST {
    pub fn new(token: Token, node: Node) -> Self {
        Self { token, node }
    }

    pub fn token(&self) -> Token {
        self.token.clone()
    }

    pub fn node(&self) -> Node {
        self.node.clone()
    }
}

#[derive(Clone)]
pub enum Operator {
    Plus,
    Minus,
    Mul,
    Div,
    Eq,
    Lt,
    Gt,
    And,
    Or,
    Not,
}

#[derive(Clone)]
pub enum Node {
    // Expression
    Value(Value),
    Identifier(String),
    UnaryOp(Operator, Box<AST>),
    BinaryOp(Operator, Box<AST>, Box<AST>),
    Assign(String, Box<AST>),
    Call(String, Box<Vec<AST>>),

    // Statement
    Empty,
    Program(Vec<Box<AST>>),
    Block(Vec<Box<AST>>),
    Expr(Box<AST>),
    VarDecl(String, Option<Box<AST>>),
    FunDecl(String, Vec<String>, Option<Box<AST>>),
    If(Box<AST>, Box<AST>, Box<Option<AST>>),
    While(Box<AST>, Box<AST>),
    Break,
    Return(Option<Box<AST>>),
}

pub trait Visitor {
    type T;

    fn visit(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Value(_) => self.visit_value(node),
            Node::Identifier(_) => self.visit_identifier(node),
            Node::UnaryOp(_, _) => self.visit_unary_op(node),
            Node::BinaryOp(_, _, _) => self.visit_binary_op(node),
            Node::Assign(_, _) => self.visit_assign(node),
            Node::Call(_, _) => self.visit_call(node),
            Node::Empty => self.visit_empty(node),
            Node::Program(_) => self.visit_program(node),
            Node::Block(_) => self.visit_block(node),
            Node::Expr(_) => self.visit_expr(node),
            Node::VarDecl(_, _) => self.visit_var_decl(node),
            Node::FunDecl(_, _, _) => self.visit_fun_decl(node),
            Node::If(_, _, _) => self.visit_if(node),
            Node::While(_, _) => self.visit_while(node),
            Node::Break => self.visit_break(node),
            Node::Return(_) => self.visit_return(node),
        }
    }

    fn visit_value(&mut self, node: &AST) -> Self::T;
    fn visit_identifier(&mut self, node: &AST) -> Self::T;
    fn visit_unary_op(&mut self, node: &AST) -> Self::T;
    fn visit_binary_op(&mut self, node: &AST) -> Self::T;
    fn visit_assign(&mut self, node: &AST) -> Self::T;
    fn visit_call(&mut self, node: &AST) -> Self::T;
    fn visit_empty(&mut self, node: &AST) -> Self::T;
    fn visit_program(&mut self, node: &AST) -> Self::T;
    fn visit_block(&mut self, node: &AST) -> Self::T;
    fn visit_expr(&mut self, node: &AST) -> Self::T;
    fn visit_var_decl(&mut self, node: &AST) -> Self::T;
    fn visit_fun_decl(&mut self, node: &AST) -> Self::T;
    fn visit_if(&mut self, node: &AST) -> Self::T;
    fn visit_while(&mut self, node: &AST) -> Self::T;
    fn visit_break(&mut self, node: &AST) -> Self::T;
    fn visit_return(&mut self, node: &AST) -> Self::T;
}
