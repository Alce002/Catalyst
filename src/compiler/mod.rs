use crate::ast::*;
use crate::error::Error;
use crate::lexer::Lexer;
use crate::token::*;
use crate::value::Value;

enum Context {
    Global,
    Function,
    Loop,
}

pub struct Compiler {
    lexer: Lexer,
    current: Option<Token>,
    context: Vec<Context>,
}

impl Compiler {
    pub fn new(lexer: Lexer) -> Self {
        Self {
            lexer,
            current: None,
            context: vec![Context::Global],
        }
    }

    pub fn compile(lexer: Lexer) -> Result<AST, Error> {
        let mut compiler = Self::new(lexer);
        compiler.advance();
        compiler.program()
    }

    fn error(&self, msg: &str) -> Error {
        if let Some(token) = self.current.as_ref() {
            match token.token_type {
                TokenType::Error => Error::ScanningError(format!(
                    "[{}:{}] {}",
                    token.line, token.column, token.lexeme
                )),
                _ => Error::CompilerError(format!("[{}:{}] {}", token.line, token.column, msg)),
            }
        } else {
            Error::CompilerError(format!("[end] {}", msg))
        }
    }

    fn advance(&mut self) {
        self.current = self.lexer.get_token();
        // println!("{}", self.current.as_ref().unwrap());
    }

    fn check(&self, token_type: TokenType) -> bool {
        if let Some(token) = self.current.as_ref() {
            std::mem::discriminant(&token.token_type) == std::mem::discriminant(&token_type)
        } else {
            false
        }
    }

    fn eat(&mut self, token_type: TokenType) -> Result<(), Error> {
        if self.check(token_type.clone()) {
            return Ok(self.advance());
        }

        Err(self.error(&format!("Expected {}", token_type)))
    }

    fn get_id(&mut self) -> Result<String, Error> {
        if let Some(token) = self.current.as_ref() {
            match token.token_type.clone() {
                TokenType::Identifier(id) => {
                    self.advance();
                    Ok(id)
                }
                _ => Err(self.error("Expected identifier")),
            }
        } else {
            Err(self.error("Expected identifier"))
        }
    }

    fn program(&mut self) -> Result<AST, Error> {
        /* {declaration} */
        let mut statements = Vec::new();

        while let Some(_) = self.current.as_ref() {
            statements.push(Box::new(self.declaration()?));
        }

        Ok(AST::new(
            Token::new(
                TokenType::Identifier("Program".to_string()),
                0,
                0,
                "Program".to_string(),
            ),
            Node::Program(statements),
        ))
    }

    fn declaration(&mut self) -> Result<AST, Error> {
        /* var_decl | fun_decl | statement */
        if let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Var => {
                    self.advance();
                    let id = self.get_id()?;

                    if self.check(TokenType::Assign) {
                        // Variable declaration and initialization
                        self.advance();
                        Ok(AST::new(
                            token,
                            Node::VarDecl(id, Some(Box::new(self.expression()?))),
                        ))
                    } else {
                        // Empty variable declaration
                        Ok(AST::new(token, Node::VarDecl(id, None)))
                    }
                }
                TokenType::Fun => {
                    // Function
                    self.advance();
                    let id = self.get_id()?;

                    self.eat(TokenType::LParen)?;

                    // Get parameters
                    let mut params = Vec::new();
                    if !self.check(TokenType::RParen) {
                        params.push(self.get_id()?);
                        while self.check(TokenType::Comma) {
                            self.advance();
                            params.push(self.get_id()?);
                        }
                    }
                    self.eat(TokenType::RParen)?;

                    // Get body
                    let body = if self.check(TokenType::LBrace) {
                        self.context.push(Context::Function);
                        let body = self.block()?;
                        self.context.pop();
                        Some(Box::new(body))
                    } else {
                        self.eat(TokenType::Semi)?;
                        None
                    };

                    Ok(AST::new(token, Node::FunDecl(id, params, body)))
                }
                _ => Ok(self.statement()?),
            }
        } else {
            Ok(self.statement()?)
        }
    }

    fn block(&mut self) -> Result<AST, Error> {
        let token = self.current.as_ref().unwrap().clone();

        let mut statements = Vec::new();
        self.advance();
        while !self.check(TokenType::RBrace) {
            statements.push(Box::new(self.declaration()?));
        }
        self.eat(TokenType::RBrace)?;
        Ok(AST::new(token, Node::Block(statements)))
    }

    fn statement(&mut self) -> Result<AST, Error> {
        if let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::LBrace => Ok(self.block()?),
                TokenType::If => {
                    self.advance();

                    self.eat(TokenType::LParen)?;
                    let expr = self.expression()?;
                    self.eat(TokenType::RParen)?;

                    let if_stmt = self.statement()?;

                    let mut else_stmt = None;
                    if self.check(TokenType::Else) {
                        self.advance();
                        else_stmt = Some(self.statement()?);
                    }

                    Ok(AST::new(
                        token,
                        Node::If(Box::new(expr), Box::new(if_stmt), Box::new(else_stmt)),
                    ))
                }
                TokenType::While => {
                    self.advance();

                    self.eat(TokenType::LParen)?;
                    let expr = self.expression()?;
                    self.eat(TokenType::RParen)?;

                    self.context.push(Context::Loop);
                    let stmt = self.statement()?;
                    self.context.pop();

                    Ok(AST::new(token, Node::While(Box::new(expr), Box::new(stmt))))
                }
                TokenType::Break => match self.context.last().unwrap() {
                    Context::Loop => {
                        self.advance();
                        self.eat(TokenType::Semi)?;
                        Ok(AST::new(token, Node::Break))
                    }
                    _ => Err(self.error("Break only allowed inside loops")),
                },
                TokenType::Return => match self.context.last().unwrap() {
                    Context::Function => {
                        self.advance();

                        let node = Node::Return(if !self.check(TokenType::Semi) {
                            Some(Box::new(self.expression()?))
                        } else {
                            None
                        });
                        self.eat(TokenType::Semi)?;

                        Ok(AST::new(token, node))
                    }
                    _ => Err(self.error("Return only allowed inside functions")),
                },
                TokenType::Semi => {
                    self.eat(TokenType::Semi)?;
                    Ok(AST::new(token, Node::Empty))
                }
                _ => {
                    let node = Node::Expr(Box::new(self.expression()?));
                    self.eat(TokenType::Semi)?;

                    Ok(AST::new(token, node))
                }
            }
        } else {
            Err(self.error("Expected statement"))
        }
    }

    fn expression(&mut self) -> Result<AST, Error> {
        Ok(self.assignment()?)
    }

    fn assignment(&mut self) -> Result<AST, Error> {
        let node = self.logical()?;
        if let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Assign => match node.node() {
                    Node::Identifier(id) => {
                        self.advance();
                        Ok(AST::new(
                            node.token(),
                            Node::Assign(id, Box::new(self.expression()?)),
                        ))
                    }
                    _ => return Err(self.error("Invalid assignment target.")),
                },
                _ => Ok(node),
            }
        } else {
            Ok(node)
        }
    }

    fn logical(&mut self) -> Result<AST, Error> {
        let mut node = self.equality()?;
        while let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::And => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::And, Box::new(node), Box::new(self.equality()?)),
                    )
                }
                TokenType::Or => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Or, Box::new(node), Box::new(self.equality()?)),
                    )
                }
                _ => break,
            }
        }
        Ok(node)
    }

    fn equality(&mut self) -> Result<AST, Error> {
        let mut node = self.comparison()?;
        while let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Eq => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Eq, Box::new(node), Box::new(self.comparison()?)),
                    )
                }
                TokenType::Neq => {
                    self.advance();
                    node = AST::new(
                        token.clone(),
                        Node::BinaryOp(Operator::Eq, Box::new(node), Box::new(self.comparison()?)),
                    );
                    node = AST::new(token, Node::UnaryOp(Operator::Not, Box::new(node)))
                }
                _ => break,
            }
        }
        Ok(node)
    }

    fn comparison(&mut self) -> Result<AST, Error> {
        let mut node = self.sum()?;
        while let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Lt => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Lt, Box::new(node), Box::new(self.sum()?)),
                    )
                }
                TokenType::Leq => {
                    self.advance();
                    let temp = AST::new(
                        token.clone(),
                        Node::BinaryOp(Operator::Gt, Box::new(node), Box::new(self.sum()?)),
                    );
                    node = AST::new(token, Node::UnaryOp(Operator::Not, Box::new(temp)))
                }
                TokenType::Gt => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Gt, Box::new(node), Box::new(self.sum()?)),
                    )
                }
                TokenType::Geq => {
                    self.advance();
                    let temp = AST::new(
                        token.clone(),
                        Node::BinaryOp(Operator::Lt, Box::new(node), Box::new(self.sum()?)),
                    );
                    node = AST::new(token, Node::UnaryOp(Operator::Not, Box::new(temp)))
                }
                _ => break,
            }
        }
        Ok(node)
    }

    fn sum(&mut self) -> Result<AST, Error> {
        let mut node = self.term()?;
        while let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Plus => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Plus, Box::new(node), Box::new(self.term()?)),
                    )
                }
                TokenType::Minus => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Minus, Box::new(node), Box::new(self.term()?)),
                    )
                }
                _ => break,
            }
        }
        Ok(node)
    }

    fn term(&mut self) -> Result<AST, Error> {
        let mut node = self.unary()?;
        while let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Mul => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Mul, Box::new(node), Box::new(self.unary()?)),
                    )
                }
                TokenType::Div => {
                    self.advance();
                    node = AST::new(
                        token,
                        Node::BinaryOp(Operator::Div, Box::new(node), Box::new(self.unary()?)),
                    )
                }
                _ => break,
            }
        }
        Ok(node)
    }

    fn unary(&mut self) -> Result<AST, Error> {
        if let Some(token) = self.current.clone() {
            match token.token_type {
                TokenType::Plus => {
                    self.advance();
                    Ok(AST::new(
                        token,
                        Node::UnaryOp(Operator::Plus, Box::new(self.unary()?)),
                    ))
                }
                TokenType::Minus => {
                    self.advance();
                    Ok(AST::new(
                        token,
                        Node::UnaryOp(Operator::Minus, Box::new(self.unary()?)),
                    ))
                }
                TokenType::Not => {
                    self.advance();
                    Ok(AST::new(
                        token,
                        Node::UnaryOp(Operator::Not, Box::new(self.unary()?)),
                    ))
                }
                _ => self.call(),
            }
        } else {
            self.call()
        }
    }

    fn call(&mut self) -> Result<AST, Error> {
        let node = self.factor()?;
        if self.check(TokenType::LParen) {
            match node.node() {
                Node::Identifier(id) => {
                    self.advance();
                    let mut arguments = Vec::new();
                    if !self.check(TokenType::RParen) {
                        arguments.push(self.expression()?);
                        while let TokenType::Comma = self.current.as_ref().unwrap().token_type {
                            self.advance();
                            arguments.push(self.expression()?);
                        }
                    }
                    self.eat(TokenType::RParen)?;
                    Ok(AST::new(node.token(), Node::Call(id, Box::new(arguments))))
                }
                _ => return Err(self.error("Invalid callee.")),
            }
        } else {
            Ok(node)
        }
    }

    fn factor(&mut self) -> Result<AST, Error> {
        if let Some(token) = self.current.clone() {
            match token.token_type.clone() {
                TokenType::Integer(n) => {
                    self.advance();
                    Ok(AST::new(token, Node::Value(Value::Integer(n as i32))))
                }
                TokenType::Boolean(b) => {
                    self.advance();
                    Ok(AST::new(token, Node::Value(Value::Boolean(b))))
                }
                TokenType::Identifier(id) => {
                    self.advance();
                    Ok(AST::new(token, Node::Identifier(id)))
                }
                TokenType::String(s) => {
                    self.advance();
                    Ok(AST::new(token, Node::Value(Value::String(s))))
                }
                TokenType::LParen => {
                    self.advance();
                    let node = self.expression()?;
                    self.advance();
                    Ok(node)
                }
                _ => Err(self.error("Expected factor")),
            }
        } else {
            Err(self.error("Expected factor"))
        }
    }
}
