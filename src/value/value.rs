use crate::error::Error;
use crate::function::Callable;
use std::fmt;
use std::rc::Rc;

pub enum Value {
    Null,
    Integer(i32),
    Boolean(bool),
    String(String),
    Function(Rc<dyn Callable>),
}

impl Clone for Value {
    fn clone(&self) -> Self {
        match self {
            Value::Null => Value::Null,
            Value::Integer(n) => Value::Integer(*n),
            Value::Boolean(b) => Value::Boolean(*b),
            Value::String(s) => Value::String(s.clone()),
            Value::Function(f) => Value::Function(f.clone()),
        }
    }
}

impl Value {
    pub const fn name(&self) -> &'static str {
        match self {
            Value::Null => "Null",
            Value::Integer(_) => "Integer",
            Value::Boolean(_) => "Boolean",
            Value::String(_) => "String",
            Value::Function(_) => "Function",
        }
    }

    fn error(lhs: &Value, rhs: &Value, op: &str) -> Error {
        Error::RuntimeError(format!(
            "{} not supported for {} and {}",
            op,
            lhs.name(),
            rhs.name()
        ))
    }

    pub fn as_bool(&self) -> bool {
        match self {
            Value::Null => false,
            Value::Integer(n) => *n != 0,
            Value::Boolean(b) => *b,
            Value::String(_) => true,
            Value::Function(_) => true,
        }
    }

    pub fn not(&self) -> Value {
        Value::Boolean(!self.as_bool())
    }

    pub fn eq(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Boolean(a == b)),
            (Value::Boolean(a), Value::Boolean(b)) => Ok(Value::Boolean(a == b)),
            _ => Err(Self::error(self, rhs, "==")),
        }
    }

    pub fn lt(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Boolean(a < b)),
            _ => Err(Self::error(self, rhs, "<")),
        }
    }

    pub fn gt(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Boolean(a > b)),
            _ => Err(Self::error(self, rhs, ">")),
        }
    }

    pub fn neg(&self) -> Result<Value, Error> {
        match self {
            Value::Integer(n) => Ok(Value::Integer(-n)),
            _ => Err(Error::RuntimeError(format!(
                "unary - not supported for {}",
                self.name()
            ))),
        }
    }

    pub fn add(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Integer(a + b)),
            _ => Err(Self::error(self, rhs, "+")),
        }
    }

    pub fn sub(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Integer(a - b)),
            _ => Err(Self::error(self, rhs, "+")),
        }
    }

    pub fn mul(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Integer(a * b)),
            _ => Err(Self::error(self, rhs, "+")),
        }
    }

    pub fn div(&self, rhs: &Value) -> Result<Value, Error> {
        match (self, rhs) {
            (Value::Integer(a), Value::Integer(b)) => Ok(Value::Integer(a / b)),
            _ => Err(Self::error(self, rhs, "+")),
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::Null => write!(f, "Null"),
            Value::Integer(n) => write!(f, "{}", n),
            Value::Boolean(b) => write!(f, "{}", *b),
            Value::String(s) => write!(f, "{}", s),
            Value::Function(func) => write!(f, "Function({})", func.arity()),
        }
    }
}
