use crate::token::*;

pub struct Lexer {
    source: Vec<char>,
    pos: usize,
    line: usize,
    column: usize,
    current: char,
    keywords: Keywords,
}

impl Lexer {
    pub fn new(source: Vec<char>) -> Self {
        let mut s = Self {
            source,
            pos: 0,
            line: 1,
            column: 1,
            current: '\0',
            keywords: Keywords::new(),
        };
        if s.source.len() > 0 {
            s.current = s.source[s.pos];
        }
        s
    }

    fn advance(&mut self) {
        self.pos += 1;
        if self.pos < self.source.len() {
            self.current = self.source[self.pos];
            self.column += 1;
            if self.current == '\n' {
                self.line += 1;
                self.column = 1;
            }
        } else {
            self.current = '\0';
        }
    }

    fn peek(&self) -> char {
        if self.pos + 1 < self.source.len() {
            self.source[self.pos + 1]
        } else {
            '\0'
        }
    }

    fn skip_whitespace(&mut self) {
        while self.current.is_whitespace() {
            self.advance();
        }
    }

    fn create_token(&mut self, token_type: TokenType, lexeme: String) -> Option<Token> {
        let line = self.line;
        let column = match token_type {
            TokenType::Error => self.column - 1,
            _ => self.column - lexeme.len(),
        };
        self.advance();
        Some(Token::new(token_type, line, column + 1, lexeme))
    }

    fn create_symbol_token(&mut self, token_type: TokenType) -> Option<Token> {
        self.create_token(token_type, self.current.to_string())
    }

    fn number(&mut self) -> Option<Token> {
        let mut value = self.current.to_string();
        while self.peek().is_digit(10) {
            self.advance();
            value.push(self.current);
        }
        self.create_token(TokenType::Integer(value.parse::<u32>().expect("Could not parse number")), value)
    }

    fn identifier(&mut self) -> Option<Token> {
        let mut s = self.current.to_string();
        while self.peek().is_alphanumeric() {
            self.advance();
            s.push(self.current);
        }
        self.create_token(self.keywords.get(s.clone()), s)
    }

    fn string(&mut self) -> Option<Token> {
        let mut s = self.current.to_string();
        while (self.peek() != '\0') & (self.peek() != '"') {
            self.advance();
            s.push(self.current);
        }
        if self.peek() != '"' {
            self.advance();
            self.create_token(TokenType::Error, format!("Unterminated string"))
        } else {
            self.advance();
            s.push(self.current);
            self.create_token(TokenType::String(s.clone()), s)
        }
    }

    pub fn get_token(&mut self) -> Option<Token> {
        self.skip_whitespace();
        match self.current {
            n if n.is_digit(10) => self.number(),
            c if c.is_alphabetic() => self.identifier(),
            '"' => self.string(),
            '+' => self.create_symbol_token(TokenType::Plus),
            '-' => self.create_symbol_token(TokenType::Minus),
            '/' => self.create_symbol_token(TokenType::Div),
            '*' => self.create_symbol_token(TokenType::Mul),
            '!' => {
                if self.peek() == '=' {
                    self.advance();
                    self.create_symbol_token(TokenType::Neq)
                } else {
                    self.create_symbol_token(TokenType::Not)
                }
            },
            '=' => {
                if self.peek() == '=' {
                    self.advance();
                    self.create_symbol_token(TokenType::Eq)
                } else {
                    self.create_symbol_token(TokenType::Assign)
                }
            },
            '<' => {
                if self.peek() == '=' {
                    self.advance();
                    self.create_symbol_token(TokenType::Leq)
                } else {
                    self.create_symbol_token(TokenType::Lt)
                }
            },
            '>' => {
                if self.peek() == '=' {
                    self.advance();
                    self.create_symbol_token(TokenType::Geq)
                } else {
                    self.create_symbol_token(TokenType::Gt)
                }
            },
            '(' => self.create_symbol_token(TokenType::LParen),
            ')' => self.create_symbol_token(TokenType::RParen),
            '}' => self.create_symbol_token(TokenType::RBrace),
            '{' => self.create_symbol_token(TokenType::LBrace),
            ';' => self.create_symbol_token(TokenType::Semi),
            ',' => self.create_symbol_token(TokenType::Comma),
            '\0' => None,
            _ => {
                self.create_token(TokenType::Error, format!("Unrecognized character '{}'", self.current))
            },
        }
    }
}
