use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;

use crate::error::Error;
use crate::function::natives::*;
use crate::value::Value;

type Env = Rc<RefCell<Environment>>;

#[derive(Default)]
pub struct Environment {
    values: HashMap<String, Option<Value>>,
    parent: Option<Rc<RefCell<Environment>>>,
}

impl Environment {
    pub fn new() -> Self {
        let mut new = Self {
            values: HashMap::new(),
            parent: None,
        };

        new.define(
            &"print".to_string(),
            Some(Value::Function(Rc::new(Print::new()))),
        );
        new.define(
            &"quit".to_string(),
            Some(Value::Function(Rc::new(Quit::new()))),
        );

        new
    }

    pub fn new_scope(parent: Env) -> Self {
        Self {
            values: HashMap::new(),
            parent: Some(parent),
        }
    }

    pub fn get_parent(&self) -> Result<Env, Error> {
        if let Some(env) = &self.parent {
            Ok(env.clone())
        } else {
            Err(Error::RuntimeError(format!("Can not exit global scope")))
        }
    }

    pub fn define(&mut self, id: &String, val: Option<Value>) {
        self.values.insert(id.clone(), val);
    }

    pub fn set(&mut self, id: &String, val: Value) -> Result<(), Error> {
        if self.values.contains_key(id) {
            self.values.insert(id.clone(), Some(val));
            Ok(())
        } else {
            if let Some(env) = self.parent.as_mut() {
                env.borrow_mut().set(id, val)
            } else {
                return Err(Error::RuntimeError(format!("{} undeclared", id)));
            }
        }
    }

    pub fn get(&self, id: &String) -> Result<Value, Error> {
        if let Some(opt) = self.values.get(id) {
            match opt {
                Some(value) => Ok((*value).clone()),
                None => Err(Error::RuntimeError(format!("{} uninitalized", id))),
            }
        } else {
            if let Some(env) = self.parent.as_ref() {
                env.borrow().get(id)
            } else {
                Err(Error::RuntimeError(format!("{} undeclared", id)))
            }
        }
    }
}
