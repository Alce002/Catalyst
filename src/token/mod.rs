use std::collections::HashMap;
use std::fmt;

#[derive(Clone)]
pub enum TokenType {
    Integer(u32), Boolean(bool),
    Identifier(String),
    String(String),
    Plus, Minus, Mul, Div,
    And, Or, Not,
    Eq, Neq, Lt, Leq, Gt, Geq,
    LParen, RParen, LBrace, RBrace,
    Assign, Semi, Comma,
    If, Else, While,
    Var, Fun,
    Break, Return,
    Error,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            Self::Integer(_) => "Integer",
            Self::Boolean(_) => "Boolean",
            Self::Identifier(_) => "Identifier",
            Self::String(_) => "String",
            Self::Plus => "Plus",
            Self::Minus => "Minus",
            Self::Mul => "Mul",
            Self::Div => "Div",
            Self::And => "And",
            Self::Or => "Or",
            Self::Not => "Not",
            Self::Eq => "Eq",
            Self::Neq => "Neq",
            Self::Lt => "Lt",
            Self::Leq => "Leq",
            Self::Gt => "Gt",
            Self::Geq => "Geq",
            Self::LParen => "LParen",
            Self::RParen => "RParen",
            Self::LBrace => "LBrace",
            Self::RBrace => "RBrace",
            Self::Assign => "Assign",
            Self::Semi => "Semi",
            Self::Comma => "Comma",
            Self::If => "If",
            Self::Else => "Else",
            Self::While => "While",
            Self::Var => "Var",
            Self::Fun => "Fun",
            Self::Break => "Break",
            Self::Return => "Return",
            Self::Error => "Error",
        })
    }
}

#[derive(Clone)]
pub struct Token {
    pub token_type: TokenType,
    pub line: usize,
    pub column: usize,
    pub lexeme: String,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<[{}] \"{}\" ({}:{})>", self.token_type, self.lexeme, self.line, self.column)
    }
}

impl Token {
    pub fn new(token_type: TokenType, line: usize, column: usize, lexeme: String) -> Self {
        Self {
            token_type,
            line,
            column,
            lexeme,
        }
    }
}

pub struct Keywords {
    keywords: HashMap<String, TokenType>,
}

impl Keywords {
    pub fn new() -> Self {
        let mut keywords = HashMap::new();
        keywords.insert("true".to_string(), TokenType::Boolean(true));
        keywords.insert("false".to_string(), TokenType::Boolean(false));
        keywords.insert("and".to_string(), TokenType::And);
        keywords.insert("or".to_string(), TokenType::Or);
        keywords.insert("var".to_string(), TokenType::Var);
        keywords.insert("fun".to_string(), TokenType::Fun);
        keywords.insert("if".to_string(), TokenType::If);
        keywords.insert("else".to_string(), TokenType::Else);
        keywords.insert("while".to_string(), TokenType::While);
        keywords.insert("return".to_string(), TokenType::Return);
        keywords.insert("break".to_string(), TokenType::Break);

        Self {
            keywords,
        }
    }

    pub fn get(&self, id: String) -> TokenType {
        if let Some(token_type) = self.keywords.get(&id) {
            token_type.clone()
        } else {
            TokenType::Identifier(id.to_string())
        }
    }
}
