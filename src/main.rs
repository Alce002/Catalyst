mod ast;
mod compiler;
mod environment;
mod error;
mod function;
mod interpreter;
mod lexer;
mod token;
mod value;

use std::env;
use std::fs::File;
use std::io::{Read, BufRead, Write, stdin, stdout};
use std::path::Path;

use ast::Visitor;
use compiler::Compiler;
use error::Error;
use interpreter::Interpreter;
use lexer::Lexer;
use value::Value;

fn repl() {
    let mut i = Interpreter::new(true);
    print!("> ");
    stdout().flush().expect("Failed to write to stdout");
    for line in stdin().lock().lines() {
        match line {
            Err(why) => panic!("Error reading from stdin: {}", why),
            Ok(l) => {
                let lexer = Lexer::new(l.as_bytes().into_iter().map(|c| *c as char).collect());

                match Compiler::compile(lexer) {
                    Ok(tree) => match i.visit(&tree) {
                        Ok(result) => {
                            if let Some(value) = result {
                                match value {
                                    Value::Null => (),
                                    _ => println!("{}", value),
                                }
                            }
                        },
                        Err(error) => match error {
                            Error::Exit(ret) => std::process::exit(ret),
                            _ => println!("{}", error),
                        }
                    },
                    Err(error) => println!("{}", error),
                }
            }
        }

        print!("> ");
        stdout().flush().expect("Failed to write to stdout");
    }
}

fn script(source: &String) {
    let path = Path::new(source);
    let mut file = match File::open(path) {
        Err(why) => panic!("Could not open {}: {}", path.display(), why),
        Ok(file) => file,
    };

    let mut v = Vec::new();
    match file.read_to_end(&mut v) {
        Err(why) => panic!("Could not read source: {}", why),
        Ok(_) => (),
    }

    let lexer = Lexer::new(v.into_iter().map(|c| c as char).collect());

    let mut i = Interpreter::new(false);

    match Compiler::compile(lexer) {
        Ok(tree) => match i.visit(&tree) {
            Ok(_) => (),
            Err(error) => match error {
                Error::Exit(ret) => std::process::exit(ret),
                _ => println!("{}", error),
            }
        },
        Err(error) => println!("{}", error),
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() == 2 {
        script(&args[1]);
    } else {
        repl();
    }
}
