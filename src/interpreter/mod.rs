use std::cell::RefCell;
use std::rc::Rc;

use crate::ast::*;
use crate::environment::Environment;
use crate::error::Error;
use crate::function::Function;
use crate::value::Value;

pub struct Interpreter {
    env: Rc<RefCell<Environment>>,
    repl: bool,
    level: u32,
}

impl Interpreter {
    pub fn new(repl: bool) -> Self {
        let env = Rc::new(RefCell::new(Environment::new()));

        Self {
            env,
            repl,
            level: 0,
        }
    }
}

impl Interpreter {
    pub fn swap_env(&mut self, env: Rc<RefCell<Environment>>) -> Rc<RefCell<Environment>> {
        let current = self.env.clone();
        self.env = Rc::new(RefCell::new(Environment::new_scope(env)));
        current
    }

    pub fn replace_env(&mut self) -> Result<(), Error> {
        self.env = self.env.take().get_parent()?;
        Ok(())
    }

    pub fn get_value(&mut self, node: &AST) -> Result<Value, Error> {
        if let Some(value) = self.visit(node)? {
            Ok(value)
        } else {
            Err(Error::RuntimeError(format!(
                "Node evaluation returned None"
            )))
        }
    }
}

impl Visitor for Interpreter {
    type T = Result<Option<Value>, Error>;

    // Expression -> Value
    fn visit_value(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Value(value) => Ok(Some(value)),
            _ => unreachable!(),
        }
    }

    fn visit_identifier(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Identifier(id) => Ok(Some(self.env.borrow().get(&id)?)),
            _ => unreachable!(),
        }
    }

    fn visit_unary_op(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::UnaryOp(op, right) => Ok(match op {
                Operator::Not => Some(self.get_value(&right)?.not()),

                Operator::Minus => Some(self.get_value(&right)?.neg()?),

                Operator::Plus => Some(self.get_value(&right)?),

                _ => return Err(Error::RuntimeError(format!("Not a valid unary operator"))),
            }),
            _ => unreachable!(),
        }
    }

    fn visit_binary_op(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::BinaryOp(op, left, right) => Ok(match op {
                Operator::Plus => Some(self.get_value(&left)?.add(&self.get_value(&right)?)?),
                Operator::Minus => Some(self.get_value(&left)?.sub(&self.get_value(&right)?)?),
                Operator::Mul => Some(self.get_value(&left)?.mul(&self.get_value(&right)?)?),
                Operator::Div => Some(self.get_value(&left)?.div(&self.get_value(&right)?)?),
                Operator::Eq => Some(self.get_value(&left)?.eq(&self.get_value(&right)?)?),
                Operator::Lt => Some(self.get_value(&left)?.lt(&self.get_value(&right)?)?),
                Operator::Gt => Some(self.get_value(&left)?.gt(&self.get_value(&right)?)?),
                Operator::And => {
                    let a = self.get_value(&left)?;
                    if !a.as_bool() {
                        Some(a)
                    } else {
                        self.visit(&right)?
                    }
                }
                Operator::Or => {
                    let a = self.get_value(&left)?;
                    if a.as_bool() {
                        Some(a)
                    } else {
                        self.visit(&right)?
                    }
                }
                _ => return Err(Error::RuntimeError(format!("Not a valid binary operator"))),
            }),
            _ => unreachable!(),
        }
    }

    fn visit_assign(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Assign(id, expr) => {
                let value = self.get_value(&expr)?;
                self.env.borrow_mut().set(&id, value.clone())?;
                Ok(Some(value))
            }
            _ => unreachable!(),
        }
    }

    fn visit_call(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Call(id, args) => {
                let func = match self.env.borrow().get(&id)? {
                    Value::Function(function) => function,
                    _ => return Err(Error::RuntimeError(format!("{} not callable", id))),
                };

                if func.arity() != args.len() {
                    return Err(Error::RuntimeError(format!(
                        "Incorrect number of arguments for function {}",
                        id
                    )));
                }

                let mut arguments = Vec::new();
                for arg in args.iter() {
                    arguments.push(self.get_value(arg)?);
                }
                Ok(Some(func.call(arguments, self)?))
            }
            _ => unreachable!(),
        }
    }

    // Statement -> None
    fn visit_empty(&mut self, _node: &AST) -> Self::T {
        Ok(None)
    }
    fn visit_program(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Program(children) => {
                let mut value = None;
                for s in &children {
                    value = self.visit(&s)?;
                }

                if self.repl && children.len() == 1 {
                    Ok(value)
                } else {
                    Ok(None)
                }
            }
            _ => unreachable!(),
        }
    }

    fn visit_block(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Block(children) => {
                self.swap_env(self.env.clone());

                let mut value = None;
                for s in &children {
                    value = self.visit(&s)?;
                }

                self.replace_env()?;

                if self.repl && self.level == 0 && children.len() == 1 {
                    Ok(value)
                } else {
                    Ok(None)
                }
            }
            _ => unreachable!(),
        }
    }

    // Returns expression value
    fn visit_expr(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Expr(expr) => self.visit(&expr),
            _ => unreachable!(),
        }
    }

    fn visit_var_decl(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::VarDecl(id, opt) => {
                let value = match opt {
                    Some(expr) => Some(self.get_value(&expr)?),
                    None => None,
                };
                self.env.borrow_mut().define(&id, value);
                Ok(None)
            }
            _ => unreachable!(),
        }
    }

    fn visit_fun_decl(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::FunDecl(id, parameters, opt) => {
                self.env.borrow_mut().define(
                    &id,
                    if let Some(body) = opt {
                        Some(Value::Function(Rc::new(Function::new(
                            *body,
                            parameters.clone(),
                            self.env.clone(),
                        ))))
                    } else {
                        None
                    },
                );
                Ok(None)
            }
            _ => unreachable!(),
        }
    }

    fn visit_if(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::If(cond, then, opt) => {
                if self.get_value(&cond)?.as_bool() {
                    self.visit(&then)?;
                } else if let Some(other) = opt.as_ref() {
                    self.visit(&other)?;
                }
                Ok(None)
            }
            _ => unreachable!(),
        }
    }

    fn visit_while(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::While(cond, expr) => {
                while self.get_value(&cond)?.as_bool() {
                    self.visit(&expr)?;
                }
                Ok(None)
            }
            _ => unreachable!(),
        }
    }

    fn visit_break(&mut self, _node: &AST) -> Self::T {
        Err(Error::Break)
    }

    fn visit_return(&mut self, node: &AST) -> Self::T {
        match node.node() {
            Node::Return(opt) => Err(Error::Return(if let Some(expr) = opt {
                self.get_value(&expr)?
            } else {
                Value::Null
            })),
            _ => unreachable!(),
        }
    }
}
