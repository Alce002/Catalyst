use std::fmt;

use crate::value::Value;

pub enum Error {
    ScanningError(String),
    CompilerError(String),
    RuntimeError(String),
    Break,
    Return(Value),
    Exit(i32),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::ScanningError(string) => format!("ScanningError: {}", string),
                Self::CompilerError(string) => format!("CompilerError: {}", string),
                Self::RuntimeError(string) => format!("RuntimeError: {}", string),
                Self::Break => "Break".to_string(),
                Self::Return(value) => format!("Return({})", value),
                Self::Exit(ret) => format!("Exit({})", ret),
            }
        )
    }
}
